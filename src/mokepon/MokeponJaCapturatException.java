package mokepon;
public class MokeponJaCapturatException extends Exception {
    public MokeponJaCapturatException(String message) {
        super(message);
    }
}