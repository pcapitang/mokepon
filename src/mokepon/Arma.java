package mokepon;

public class Arma extends Objecte implements Equipament {
	int atacExtra;
	
	Arma(String nom, int atkEx) {
		super(nom);
		this.atacExtra = atkEx;
	}

	@Override
	public void utilitzar(Mokepon mok) {
	}

	@Override
	public void Equipar(MokeponCapturat mokcap) {
		mokcap.objecteEquipat = this;
		mokcap.setAtk(mokcap.getAtk()+atacExtra);
	}
	public void Desequipar(MokeponCapturat mokcap) {
		mokcap.objecteEquipat = null;
		mokcap.setAtk(mokcap.getAtk()-atacExtra);
	}

}
