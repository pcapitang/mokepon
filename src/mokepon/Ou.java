package mokepon;
public class Ou {
	private String especie;
	private Tipus Tipus;
	private int pasesRestants;

	public Ou(String esp, Tipus tip) {
		this.especie = esp;
		this.Tipus = tip;
		this.pasesRestants = (int) (Math.random() * 10 + 5);
	}
	public void caminar() {
		this.pasesRestants -= 1;
		if(this.pasesRestants <= 0) {
			eclosionar(this.especie, this.Tipus);
		}
	}
	public Mokepon eclosionar(String nom, Tipus tip) {
		Mokepon volatil = new Mokepon(nom, tip);
		return volatil;
	}

}