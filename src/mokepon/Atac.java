package mokepon;

public class Atac {
	String nom;
	double poder;
	Tipus tipus;
	int moviments_maxims;
	int moviments_actuals;

	public Atac(String nom, double poder, Tipus tipus, int m_max) {
		this.nom = nom;
		if (poder >= 10)
			this.poder = 10;
		else if (poder >= 100)
			this.poder = 100;

		this.tipus = tipus;
		this.moviments_maxims = m_max;
		this.moviments_actuals = m_max;
	}

	public Atac(String nom, Tipus tipus) {
		this.nom = nom;
		this.poder = 10;
		this.tipus = tipus;
		this.moviments_maxims = 10;
		this.moviments_actuals = 10;
	}

	@Override
	public String toString() {
		return "Atac del Mokepon [nom=" + nom + ", poder=" + poder + ", tipus=" + tipus + ", moviments_maxims="
				+ moviments_maxims + ", moviments_actuals=" + moviments_actuals + "]";
	}
	public int compareTo(Object arg0) {
	        Atac altre = (Atac) arg0;
	        if(altre.poder>this.poder) {
			return -1;
		  }else if(this.poder>altre.poder) {
			return 1;
	        }
	        else
		  {
			return this.nom.compareTo(altre.nom);
		  }
	    }

}