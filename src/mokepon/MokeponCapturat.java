package mokepon;
public class MokeponCapturat extends Mokepon {
    String nomPosat;
    private String nomEntrenador;
    private int felicitat;
    static  int NumMokCapturats;
    Objecte obj;
    Equipament objecteEquipat;

	public MokeponCapturat() {
		NumMokCapturats++;
    }
    public MokeponCapturat(String nom, Tipus tipus) {
        super(nom, tipus);
        this.nomPosat = nom;
        this.setNomEntrenador("Pablo");
        this.setFelicitat(50);
        NumMokCapturats++;
    }
    public MokeponCapturat(Mokepon mok, String nomPosat, String nomEntrenador) {
    	super(mok.getNom(), mok.getNivell(), mok.getHp_max(), mok.getAtk(), mok.getDef(), mok.getVel(), mok.getTipus1(), mok.getTipus2());
    	this.setSex(mok.getSex());
    	this.nomPosat = nomPosat;
        this.setNomEntrenador(nomEntrenador);
        this.setFelicitat(50);
        NumMokCapturats++;
    }
    public void acariciar(MokeponCapturat mok) {
    	this.felicitat+=10;
    	if (this.felicitat > 100)
            this.setFelicitat(100);
    }
    public void utilitzaObjecte(MokeponCapturat mok){
    	mok.obj.utilitzar(mok);
    }
    @Override
	public void atacar(Mokepon atacat, int num_atac) {
		if (!atacat.devilitat) {
			Atac aVolatil = this.atac.get(num_atac);
			double damage = (int) (((((((2 * this.getNivell()) / 5) + 2) * aVolatil.poder * this.getDef()/ atacat.getDef()) / 50) + 2)
			* efectivitat(aVolatil.tipus, atacat.getTipus1()));
			if(this.felicitat >= 50)
				damage = damage * 1.2;
			else 
				damage = damage * 0.8;
			
			atacat.setHp_actual(atacat.getHp_actual() - (int)damage);
			if (atacat.getHp_actual() <= 0) {
				atacat.setHp_actual(0);
				atacat.devilitat = true;
			}
		}
	}
	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}
	public String getNomEntrenador() {
		return nomEntrenador;
	}
	public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
	}
    public static int getNumMokCapturats() {
		return NumMokCapturats;
	}
    
    @Override
	public boolean equals(Object obj) {
		MokeponCapturat other = (MokeponCapturat) obj;
		if (super.equals(obj) && this.nomPosat == other.nomPosat && this.getNomEntrenador() == other.getNomEntrenador()) {
			return true;
		} else {
			return false;
		}
	}
}
