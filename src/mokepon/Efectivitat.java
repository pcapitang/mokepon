package mokepon;
public class Efectivitat {
	public static double devilidades(Tipus atac, Tipus defensa) {
			if (atac == Tipus.FUEGO && defensa == Tipus.AGUA // resistencias
					|| atac == Tipus.FUEGO && defensa == Tipus.ROCA
					|| atac == Tipus.FUEGO && defensa == Tipus.FUEGO
					|| atac == Tipus.FUEGO && defensa == Tipus.DRAGON 
					|| atac == Tipus.AGUA && defensa == Tipus.PLANTA
					|| atac == Tipus.AGUA && defensa == Tipus.AGUA 
					|| atac == Tipus.AGUA && defensa == Tipus.DRAGON
					|| atac == Tipus.PLANTA && defensa == Tipus.VOLADOR 
					|| atac == Tipus.PLANTA && defensa == Tipus.VENENO
					|| atac == Tipus.PLANTA && defensa == Tipus.BICHO 
					|| atac == Tipus.PLANTA && defensa == Tipus.ACERO
					|| atac == Tipus.PLANTA && defensa == Tipus.PLANTA 
					|| atac == Tipus.PLANTA && defensa == Tipus.DRAGON
					|| atac == Tipus.PLANTA && defensa == Tipus.FUEGO 
					|| atac == Tipus.NORMAL && defensa == Tipus.ROCA
					|| atac == Tipus.NORMAL && defensa == Tipus.ACERO 
					|| atac == Tipus.LUCHA && defensa == Tipus.VOLADOR
					|| atac == Tipus.LUCHA && defensa == Tipus.VENENO 
					|| atac == Tipus.LUCHA && defensa == Tipus.BICHO
					|| atac == Tipus.LUCHA && defensa == Tipus.PSIQUICO 
					|| atac == Tipus.LUCHA && defensa == Tipus.HADA
					|| atac == Tipus.VOLADOR && defensa == Tipus.ROCA 
					|| atac == Tipus.VOLADOR && defensa == Tipus.ACERO
					|| atac == Tipus.VOLADOR && defensa == Tipus.ELECTRICO
					|| atac == Tipus.VENENO && defensa == Tipus.VENENO 
					|| atac == Tipus.VENENO && defensa == Tipus.TIERRA
					|| atac == Tipus.VENENO && defensa == Tipus.ROCA 
					|| atac == Tipus.VENENO && defensa == Tipus.FANTASMA
					|| atac == Tipus.TIERRA && defensa == Tipus.BICHO 
					|| atac == Tipus.TIERRA && defensa == Tipus.PLANTA
					|| atac == Tipus.TIERRA && defensa == Tipus.BICHO 
					|| atac == Tipus.ROCA && defensa == Tipus.LUCHA
					|| atac == Tipus.ROCA && defensa == Tipus.TIERRA 
					|| atac == Tipus.ROCA && defensa == Tipus.ACERO
					|| atac == Tipus.BICHO && defensa == Tipus.LUCHA 
					|| atac == Tipus.BICHO && defensa == Tipus.VOLADOR
					|| atac == Tipus.BICHO && defensa == Tipus.VENENO 
					|| atac == Tipus.BICHO && defensa == Tipus.FANTASMA
					|| atac == Tipus.BICHO && defensa == Tipus.ACERO 
					|| atac == Tipus.BICHO && defensa == Tipus.FUEGO
					|| atac == Tipus.BICHO && defensa == Tipus.HADA 
					|| atac == Tipus.FANTASMA && defensa == Tipus.SINIESTRO
					|| atac == Tipus.ACERO && defensa == Tipus.ACERO 
					|| atac == Tipus.ACERO && defensa == Tipus.FUEGO
					|| atac == Tipus.ACERO && defensa == Tipus.AGUA 
					|| atac == Tipus.ACERO && defensa == Tipus.ELECTRICO
					|| atac == Tipus.ELECTRICO && defensa == Tipus.PLANTA
					|| atac == Tipus.ELECTRICO && defensa == Tipus.ELECTRICO
					|| atac == Tipus.ELECTRICO && defensa == Tipus.DRAGON
					|| atac == Tipus.PSIQUICO && defensa == Tipus.ACERO
					|| atac == Tipus.PSIQUICO && defensa == Tipus.PSIQUICO 
					|| atac == Tipus.HIELO && defensa == Tipus.ACERO
					|| atac == Tipus.HIELO && defensa == Tipus.FUEGO 
					|| atac == Tipus.HIELO && defensa == Tipus.AGUA
					|| atac == Tipus.HIELO && defensa == Tipus.HIELO 
					|| atac == Tipus.DRAGON && defensa == Tipus.ACERO
					|| atac == Tipus.SINIESTRO && defensa == Tipus.LUCHA
					|| atac == Tipus.SINIESTRO && defensa == Tipus.SINIESTRO
					|| atac == Tipus.SINIESTRO && defensa == Tipus.HADA 
					|| atac == Tipus.HADA && defensa == Tipus.VENENO
					|| atac == Tipus.HADA && defensa == Tipus.ACERO 
					|| atac == Tipus.HADA && defensa == Tipus.FUEGO) {
				return 0.5;
			} else if (atac == Tipus.AGUA && defensa == Tipus.FUEGO // supereficaz
					|| atac == Tipus.AGUA && defensa == Tipus.TIERRA
					|| atac == Tipus.AGUA && defensa == Tipus.ROCA
					|| atac == Tipus.FUEGO && defensa == Tipus.PLANTA 
					|| atac == Tipus.FUEGO && defensa == Tipus.BICHO
					|| atac == Tipus.FUEGO && defensa == Tipus.ACERO 
					|| atac == Tipus.FUEGO && defensa == Tipus.HIELO
					|| atac == Tipus.PLANTA && defensa == Tipus.AGUA 
					|| atac == Tipus.PLANTA && defensa == Tipus.ROCA
					|| atac == Tipus.PLANTA && defensa == Tipus.TIERRA 
					|| atac == Tipus.LUCHA && defensa == Tipus.NORMAL
					|| atac == Tipus.LUCHA && defensa == Tipus.ROCA 
					|| atac == Tipus.LUCHA && defensa == Tipus.ACERO
					|| atac == Tipus.LUCHA && defensa == Tipus.HIELO 
					|| atac == Tipus.LUCHA && defensa == Tipus.SINIESTRO
					|| atac == Tipus.VOLADOR && defensa == Tipus.LUCHA 
					|| atac == Tipus.VOLADOR && defensa == Tipus.BICHO
					|| atac == Tipus.VOLADOR && defensa == Tipus.PLANTA 
					|| atac == Tipus.VENENO && defensa == Tipus.PLANTA
					|| atac == Tipus.VENENO && defensa == Tipus.HADA 
					|| atac == Tipus.TIERRA && defensa == Tipus.VENENO
					|| atac == Tipus.TIERRA && defensa == Tipus.ROCA 
					|| atac == Tipus.TIERRA && defensa == Tipus.ACERO
					|| atac == Tipus.TIERRA && defensa == Tipus.FUEGO 
					|| atac == Tipus.TIERRA && defensa == Tipus.ELECTRICO
					|| atac == Tipus.ROCA && defensa == Tipus.VOLADOR 
					|| atac == Tipus.ROCA && defensa == Tipus.BICHO
					|| atac == Tipus.ROCA && defensa == Tipus.FUEGO 
					|| atac == Tipus.ROCA && defensa == Tipus.HIELO
					|| atac == Tipus.BICHO && defensa == Tipus.PLANTA 
					|| atac == Tipus.BICHO && defensa == Tipus.PLANTA
					|| atac == Tipus.BICHO && defensa == Tipus.PSIQUICO 
					|| atac == Tipus.BICHO && defensa == Tipus.SINIESTRO
					|| atac == Tipus.FANTASMA && defensa == Tipus.FANTASMA
					|| atac == Tipus.FANTASMA && defensa == Tipus.PSIQUICO 
					|| atac == Tipus.ACERO && defensa == Tipus.ROCA
					|| atac == Tipus.ACERO && defensa == Tipus.HIELO 
					|| atac == Tipus.ACERO && defensa == Tipus.HADA
					|| atac == Tipus.ELECTRICO && defensa == Tipus.VOLADOR
					|| atac == Tipus.ELECTRICO && defensa == Tipus.AGUA 
					|| atac == Tipus.PSIQUICO && defensa == Tipus.LUCHA
					|| atac == Tipus.PSIQUICO && defensa == Tipus.VENENO 
					|| atac == Tipus.PSIQUICO && defensa == Tipus.LUCHA
					|| atac == Tipus.HIELO && defensa == Tipus.VOLADOR 
					|| atac == Tipus.HIELO && defensa == Tipus.TIERRA
					|| atac == Tipus.HIELO && defensa == Tipus.PLANTA 
					|| atac == Tipus.HIELO && defensa == Tipus.DRAGON
					|| atac == Tipus.DRAGON && defensa == Tipus.DRAGON
					|| atac == Tipus.SINIESTRO && defensa == Tipus.FANTASMA
					|| atac == Tipus.SINIESTRO && defensa == Tipus.PSIQUICO 
					|| atac == Tipus.HADA && defensa == Tipus.LUCHA
					|| atac == Tipus.HADA && defensa == Tipus.DRAGON 
					|| atac == Tipus.HADA && defensa == Tipus.SINIESTRO
					|| atac == Tipus.HADA && defensa == Tipus.LUCHA) {
				return 2;
			} else if (atac == Tipus.NORMAL && defensa == Tipus.FANTASMA // inmunidades
					|| atac == Tipus.FANTASMA && defensa == Tipus.NORMAL 
					|| atac == Tipus.LUCHA && defensa == Tipus.FANTASMA
					|| atac == Tipus.VENENO && defensa == Tipus.ACERO 
					|| atac == Tipus.TIERRA && defensa == Tipus.VOLADOR
					|| atac == Tipus.ELECTRICO && defensa == Tipus.TIERRA
					|| atac == Tipus.LUCHA && defensa == Tipus.FANTASMA
					|| atac == Tipus.PSIQUICO && defensa == Tipus.SINIESTRO
					|| atac == Tipus.DRAGON && defensa == Tipus.HADA) {
				return 0;
			} else {
				return 1; // neutro
			}
		}
}
