package mokepon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mokepon implements java.lang.Comparable<Mokepon>{
	// atributs del pokemon
	private String nom;
	public Random rd = new Random();
	private int nivell;
	private int atk;
	private int def;
	private int vel;
	private int exp;
	private int hp_max;
	private int hp_actual;
	private Tipus tipus1;
	private Tipus tipus2; /*
							 * Els pokemon poden tenir un tipus secundari. Si no hi tenen tipus secundari,
							 * aquest adquirira el tipus primari
							 */
	List<Atac> atac = new ArrayList<Atac>();
	boolean devilitat;
	static String estatic;
	private Sexe sex;
	Random rnd = new Random();

	public Mokepon() {
		this.nom = "Sense definir";
		this.hp_max = 10;
		this.hp_actual = 10;
		this.nivell = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.tipus1 = null;
		this.tipus2 = null;
		this.devilitat = false;
		this.sex = rnd.nextBoolean() ? Sexe.M : Sexe.F;
	}

	public Mokepon(String nom) {
		this.nom = nom;
		this.hp_max = 10;
		this.hp_actual = 10;
		this.nivell = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.tipus1 = null;
		this.tipus2 = null;
		this.devilitat = false;
		this.sex = rnd.nextBoolean() ? Sexe.M : Sexe.F;
	}

	public Mokepon(String nom, int nivell, int hp_max, int atk, int def, int vel, Tipus tipus) {
		this.nom = nom;
		this.hp_max = hp_max;
		this.hp_actual = hp_max;
		this.nivell = nivell;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.tipus1 = tipus;
		this.tipus2 = tipus;
		this.devilitat = false;
		this.sex = rnd.nextBoolean() ? Sexe.M : Sexe.F;
	}

	public Mokepon(String nom, int nivell, int hp_max, int atk, int def, int vel, Tipus tipus1, Tipus tipus2) {
		this.nom = nom;
		this.hp_max = hp_max;
		this.hp_actual = hp_max;
		this.nivell = nivell;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.tipus1 = tipus1;
		this.tipus2 = tipus2;
		this.devilitat = false;
		this.sex = rnd.nextBoolean() ? Sexe.M : Sexe.F;
	}

	public Mokepon(String nom, int nivell) {
		this(nom);
		for (int i = 1; i < nivell; i++) {
			this.pujarNivell();
		}
	}

	public Mokepon(String nom, Tipus tipus) {
		this.nom = nom;
		this.tipus1 = tipus;
	}

	public Mokepon(String nom, Tipus tipus1, Tipus tipus2) {
		this.nom = nom;
		this.tipus1 = tipus1;
		this.tipus1 = tipus2;
	}

	public void diguesNom() {
		System.out.println(this.nom);
	}

	public void atorgarExperiencia(int exp_otorgada) {
		this.exp += exp_otorgada;
		pujarNivell();
	}

	public void pujarNivell() {
		nivell += 1;
		int hp = rd.nextInt(6);
		this.hp_max += hp;
		this.hp_actual = hp_actual + hp; /*
											 * Si el pokemon no te el maxim de la seva vida es suma els punts de vida
											 * que te ara mes els que obte al pujar de nivell
											 */
		this.atk += rd.nextInt(3);
		this.def += rd.nextInt(3);
		this.vel += rd.nextInt(3);
	}

	public void afegirAtac(Atac at) {
		if (this.atac.size() < 2) {
			this.atac.add(at);
		}
	}

	public void atacar(Mokepon atacat, int num_atac) {
		if (!atacat.devilitat && this.atac.get(num_atac).moviments_actuals > 0) {
			Atac aVolatil = this.atac.get(num_atac);
			int damage = (int) (((((((2 * this.nivell) / 5) + 2) * aVolatil.poder * this.atk / atacat.def) / 50) + 2)
					* efectivitat(aVolatil.tipus, atacat.tipus1));
			atacat.hp_actual -= damage;
			if (atacat.hp_actual <= 0) {
				atacat.hp_actual = 0;
				atacat.devilitat = true;
			}
			this.atac.get(num_atac).moviments_actuals -= 1;
		}
	}

	public MokeponCapturat capturar(String nomEntrenador, String nomDonat) {
		MokeponCapturat volatil = new MokeponCapturat();
		if (!(this instanceof MokeponCapturat)) {
			volatil.setNomEntrenador(nomEntrenador);
			volatil.nomPosat = nomDonat;
			volatil.setAtk(this.atk);
			volatil.setDef(this.def);
			volatil.setExp(this.exp);
			volatil.setFelicitat(50);
			volatil.setHp_max(this.hp_max);
			volatil.setHp_actual(this.hp_actual);
			volatil.setNom(this.nom);
			MokeponCapturat.NumMokCapturats++;
		} else {
			System.out.println("No pots capturar un Mokepon que ja esta capturat");
		}
		return volatil;
	}

	static MokeponCapturat capturar(Mokepon mok, String nomEntrenador, String nomDonat)
			throws MokeponJaCapturatException {
		MokeponCapturat volatil = new MokeponCapturat();
		if (!(mok instanceof MokeponCapturat)) {
			volatil.setNomEntrenador(nomEntrenador);
			volatil.nomPosat = nomDonat;
			volatil.setAtk(mok.atk);
			volatil.setDef(mok.def);
			volatil.setExp(mok.exp);
			volatil.setFelicitat(50);
			volatil.setHp_max(mok.hp_max);
			volatil.setHp_actual(mok.hp_actual);
			volatil.setNom(mok.nom);
			volatil.setSex(mok.sex);
			MokeponCapturat.NumMokCapturats++;
		} else {
			throw new MokeponJaCapturatException("Error, No pots capturar un Mokepon ja capturat");
		}
		return (MokeponCapturat) volatil;
	}

	public Ou reproduccio(Mokepon altreMok) throws Exception {
		if ((this.tipus1 == altreMok.tipus1)
				&& (this.getSex() != altreMok.getSex() && (!this.devilitat) && (!altreMok.devilitat))) {
			Ou volatil = new Ou(rnd.nextBoolean() ? this.getNom() : altreMok.getNom(), this.tipus1);
			return volatil;
		} else if (this.tipus1 != altreMok.tipus1)
			throw new TipusDiferentException("Error, son de diferent tipus");
		else if (this.getSex() == altreMok.getSex())
			throw new SexeIgualException("Error, son del mateix sexe");
		else
			throw new MokeponEsDevilitat("Error, un o els dos es devilitat");
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNivell() {
		return nivell;
	}

	public void setNivell(int nivell) {
		this.nivell = nivell;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getVel() {
		return vel;
	}

	public void setVel(int vel) {
		this.vel = vel;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getHp_max() {
		return hp_max;
	}

	public void setHp_max(int hp_max) {
		this.hp_max = hp_max;
	}

	public int getHp_actual() {
		return hp_actual;
	}

	public void setHp_actual(int hp_actual) {
		this.hp_actual = hp_actual;
		if (this.hp_actual < 0) {
			this.hp_actual = 0;
		}
		if (this.hp_actual > this.hp_max) {
			this.hp_actual = this.hp_max;
		}
	}

	public Tipus getTipus1() {
		return tipus1;
	}

	public void setTipus1(Tipus tipus) {
		this.tipus1 = tipus;
	}

	public Tipus getTipus2() {
		return tipus2;
	}

	public void setTipus2(Tipus tipus) {
		this.tipus2 = tipus;
	}

	public List<Atac> getAtac() {
		return atac;
	}

	public Sexe getSex() {
		return sex;
	}

	public void setSex(Sexe sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Mokepon [nom=" + nom + ", nivell=" + nivell + ", atk=" + atk + ", def=" + def + ", vel=" + vel
				+ ", exp=" + exp + ", hp_max=" + hp_max + ", hp_actual=" + hp_actual + "]\n";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		Mokepon other = (Mokepon) obj;
		if (this.getAtk() == other.getAtk() && this.getDef() == other.getDef() && this.getVel() == other.getVel()
				&& this.getSex() == other.getSex() && this.getNom() == other.getNom()
				&& this.getNivell() == other.getNivell() && this.getHp_max() == other.getHp_max()
				&& this.getTipus1() == other.getTipus1() && this.getTipus2() == other.getTipus2()) {
			return true;
		} else {
			return false;
		}
	}

	public double efectivitat(Tipus atac, Tipus defensa) {
		return Efectivitat.devilidades(atac, defensa);
	}
	@Override
	public int compareTo(Mokepon arg0) {
		Mokepon other = (Mokepon) arg0;
		if (other.tipus1 != this.tipus1)
			return -1;
		else if (this.getNom() != other.getNom())
			return this.getNom().compareTo(other.getNom());
		else if (this.getNivell() > other.getNivell())
			return 1;
		else
			return 0;
	}

}
