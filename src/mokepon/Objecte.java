package mokepon;

public abstract class Objecte {
	private String nom;
	protected int quantitat;
	
	Objecte(String nom){
		this.nom = nom;
		this.quantitat = 1;
	}
	
	public void obtenir(int numObjectes){
		this.quantitat++;
	}
	public void donar(MokeponCapturat mokcap){
		mokcap.obj = this;
	}
	public abstract void utilitzar(Mokepon mok);
	
	public String getNom() {
		return nom;
	}
	public int getQuantitat() {
		return quantitat;
	}
	
}