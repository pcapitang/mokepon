package mokepon;

import java.util.Random;
import java.util.Scanner;

public class Combat {
	static Scanner sc = new Scanner(System.in);
	static Random rnd = new Random();
	public static void combate(Mokepon mokepon1, Mokepon mokepon2) {
		boolean fi = false;
		int torn = MokeponMesRapid(mokepon1,mokepon2);
	        while(!fi) {
	                int numAtac = triaAtac(mokepon1,mokepon2);
	                boolean debilitat = gestionarAtac(mokepon1, mokepon2, torn, numAtac);
	                fi = fiCombat(debilitat, torn);
	                torn = canviTorn(torn);
	        }
	}

	private static int canviTorn(int torn) {
		if(torn == 1)
			return 2;
		else 
			return 1;
	}

	private static boolean fiCombat(boolean debilitat, int torn) {
		if(debilitat && torn == 1) {
			System.out.println("El pokemon 1 ha guanyat!");
			return true;
		}
		else if (debilitat && torn == 2) {
			System.out.println("El pokemon 2 ha guanyat!");
			return true;
		}
		return false;
	}

	private static boolean gestionarAtac(Mokepon mokepon1, Mokepon mokepon2, int torn, int numAtac) {
		if(torn == 1) {
		mokepon1.atacar(mokepon2, numAtac);
        if(mokepon2.devilitat)
        	return true;
        else
        	return false;
		}
		else {
		mokepon2.atacar(mokepon1, numAtac);
        if(mokepon1.devilitat)
        	return true;
        else
        	return false;
		}
	}

	private static int triaAtac(Mokepon mok1, Mokepon mok2) {
		if(mok1 instanceof MokeponCapturat) {
			System.out.println("Seleciona l'atac del teu Mokepon");
			System.out.println("0 = Primer atac / 1 = Segon atac");
			System.out.println(mok1.atac);
			int numAtac = sc.nextInt();
				return numAtac;
		}
		
		if(mok2 instanceof MokeponCapturat) {
			System.out.println("Seleciona l'atac del teu Mokepon");
			System.out.println("0 = Primer atac / 1 = Segon atac");
			System.out.println(mok2.atac);
			int numAtac = sc.nextInt();
				return numAtac;
		}
		else {
			System.out.println("El mokepon salvatge ha atacat!");
			int numAtac2 = rnd.nextBoolean() ? 0 : 1;
			return numAtac2;
		}
	}

	private static int MokeponMesRapid(Mokepon mokepon1, Mokepon mokepon2) {
		if(mokepon1.getVel() > mokepon2.getVel())
			return 1;
		else
			return 0;
	}
}

