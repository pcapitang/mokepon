package mokepon;

public class Pocio extends Objecte {
	public int hp_curada;

	Pocio(String nom, int hp_curada) {
		super(nom);
		this.quantitat = 1;
		this.hp_curada = hp_curada;
		
	}

	public void utilitzar(Mokepon mok) {
		if (super.getQuantitat() >= 1 && !mok.devilitat) {
			mok.setHp_actual(mok.getHp_actual() + hp_curada);
			if (mok.getHp_actual() > mok.getHp_max())
				mok.setHp_actual(mok.getHp_max());
		}this.quantitat = (this.getQuantitat()-1);
	}
}