package mokepon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Test2{

	public static void main(String[] args) {
	/*Tipos*/
	/*Mokepon marmander = new Mokepon("Marmander", Tipus.FUEGO);
   	MokeponCapturat elMeuMikachu = new MokeponCapturat();
    elMeuMikachu.nomPosat = "Mokocho";
    System.out.println(elMeuMikachu.nomPosat);
    elMeuMikachu.diguesNom();*/
	/*Prueva estatic*/
    Mokepon mikachu = new Mokepon("Mikachu", 1, 12, 10, 10, 3, Tipus.ELECTRICO);
    mikachu.estatic = "hola";
    Mokepon mulmasaur = new Mokepon("Mulmasaur", 1, 15, 10, 15, 2, Tipus.PLANTA, Tipus.VENENO);
    mulmasaur.estatic = "adeu";
    //System.out.println(mikachu.estatic);         
   
    /*Prueva estatic - Numero de mokepons*/  
    MokeponCapturat elMeuMikachu = new MokeponCapturat(mikachu,"Mokocho","Pablo");
    MokeponCapturat elMeuMulmasur = new MokeponCapturat(mulmasaur,"Molmosour","Pablo");
    //System.out.println(MokeponCapturat.getNumMokCapturats());
    
    /*Objetos*/
    /*
    elMeuMulmasur.devilitat = true;
  	System.out.println("Antes: " + elMeuMulmasur.devilitat);  
    */
    /*Revivimos a mulmasur*/
    /*
    Reviure reviure = new Reviure("Reviure");
	reviure.utilitzar(elMeuMulmasur);
	System.out.println("Despues: " + elMeuMulmasur.devilitat);  
	System.out.println("Vida: " + elMeuMulmasur.getHp_actual());
    */
    /*Curamos a mulmasur*/
	/*
    Pocio pocio = new Pocio("Pocio",20);
    pocio.utilitzar(elMeuMulmasur);
	System.out.println("Vida amb pocio: " + elMeuMulmasur.getHp_actual());
    */
    /*Equipamos a mulmasur un arma*/
    /*
   	System.out.println("\nAtaque de mulmasur sin arma: "+ elMeuMulmasur.getAtk());
    Arma shotgun = new Arma("Escopeta",10);
    shotgun.Equipar(elMeuMulmasur);
   	System.out.println("Ataque de mulmasur con arma: "+ elMeuMulmasur.getAtk());
    shotgun.Desequipar(elMeuMulmasur);
    */
    /*Equipamos a mulmasur un Armadura*/
    /*
   // System.out.println("Defensa de mulmasur sin Armadura: "+ elMeuMulmasur.getDef());
    Armadura chalecoAsalto = new Armadura("Escopeta",10);
    chalecoAsalto.Equipar(elMeuMulmasur);
   // System.out.println("Defensa de mulmasur con Armadura: "+ elMeuMulmasur.getDef());
    */
    
    /*Pruevas Ou*/
    Mokepon moswuneet = new Mokepon("Moswuneet", 1, 10, 1, 1, 1, Tipus.PLANTA);
    /*Pot donar la coinidencia de que son del mateix genere pero aixo funciona si son Mascle i Femella*/
 	//System.out.println("\nIntent amb mateix tipus (Pot donar error per el sexe): ");
    /*try {
        Ou marmasaur = mulmasaur.reproduccio(moswuneet);
       	System.out.println(mulmasaur.getNom()+ " està molt cansat");
       } catch (Exception err) {
         	System.out.println(err.getMessage());
       }
       */
    /*Es queixara sempre del tipus*/
  	/*System.out.println("Intent amb diferent tipus: ");
    try {
        Ou marmasaur = mulmasaur.reproduccio(mikachu);
       	System.out.println(mulmasaur.getNom()+ " està molt cansat");
       } catch (Exception err) {
         	System.out.println(err.getMessage());
       }
    */
    /*Proves Comparadors - Equals*/
    ArrayList<Mokepon> mokedex = new ArrayList<Mokepon>();
    mokedex.add(moswuneet);
    mokedex.add(mulmasaur);
    mokedex.add(mikachu);
    Collections.sort(mokedex);
    //Donara error
    for (int i = 0; i < mokedex.size(); i++) {
        //System.out.println(mokedex.get(i));
      }
    
    
    /*Vamos a proar el sistema de combate*/
    /*Primero añadimos ataques a dos pokemons salvajes y a uno nuestro*/
    Atac leachSeed = new Atac("Drenadoras", Tipus.PLANTA);
    elMeuMulmasur.afegirAtac(leachSeed);
    Atac leafBlade = new Atac("Hoja aguda", Tipus.PLANTA);
    elMeuMulmasur.afegirAtac(leafBlade);
    
    Atac thunderbolt  = new Atac("Rayo", Tipus.ELECTRICO);
    mikachu.afegirAtac(thunderbolt);
    Atac quickatack = new Atac("Ataque rapido", Tipus.NORMAL);
    mikachu.afegirAtac(quickatack);
    
    Atac gigaDrain  = new Atac("Gigadrenado", Tipus.PLANTA);
    moswuneet.afegirAtac(gigaDrain);
    Atac leafStorm = new Atac("Lluevehojas", Tipus.PLANTA);
    moswuneet.afegirAtac(leafStorm);
    
    
   // Combat.combate(elMeuMulmasur,mikachu);
    
    
    //Combat.combate(moswuneet,mikachu); // aqui guañara sempre mikachu
    
    Combat.combate(elMeuMulmasur,mikachu);
    
	}
}
