package mokepon;

public class Armadura extends Objecte implements Equipament {
	int defensaExtra;
	
	Armadura(String nom, int defEx) {
		super(nom);
		this.defensaExtra = defEx;
	}

	@Override
	public void utilitzar(Mokepon mok) {
	}

	@Override
	public void Equipar(MokeponCapturat mokcap) {
		mokcap.objecteEquipat = this;
		mokcap.setDef(mokcap.getDef()+defensaExtra);
	}
	public void Desequipar(MokeponCapturat mokcap) {
		mokcap.objecteEquipat = null;
		mokcap.setDef(mokcap.getDef()-defensaExtra);
	}
	
}
