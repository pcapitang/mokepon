package Fixers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ficher1 {
	public static void afegirGimnas(String nomGimnas, String ciutat, String liderGimnas) throws GimnasExiteix {
		try {
			File f = new File("gimnasos.txt");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line = "";
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);
			while (br.ready()) {
				line = br.readLine();
				String[] a = line.split(";");
				if (a[0].equals(nomGimnas)) {
					br.close();
					throw new GimnasExiteix("El gimnas " + nomGimnas + " ja existeix en el fitxer.");
				}
			}
			bw.append(nomGimnas + ";" + ciutat + ";" + liderGimnas + ";" + 0 + "\n");
			bw.flush();
			bw.close();
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
			e.printStackTrace();
		}
	}

	public static void mostraGimnasos() {
		try {
			File f = new File("gimnasos.txt");
			FileReader fr;
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				System.out.println(br.readLine());
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Excepció general de lectura");
			e.printStackTrace();
		}
	}

	public static void cercaLider(String nomGimnas) throws IOException {
		File f = new File("gimnasos.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		while (br.ready()) {
			String s = br.readLine();
			if (s.contains(nomGimnas)) {
				String[] a = s.split(";");
				System.out.println(a[2]);
			}
		}
		br.close();
	}

	public static void invictes(int n) throws IOException {
		File f = new File("gimnasos.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		while (br.ready()) {
			String s = br.readLine();
			String[] a = s.split(";");
			if (Integer.parseInt(a[3]) == (n)) {
				System.out.println(br.readLine());
			}
		}
		br.close();
	}

	public static void copiaSeguretat(String path1, String path2) throws Exception {
		FileInputStream in = new FileInputStream(path1);
		FileOutputStream out = new FileOutputStream(path2);
		try {
			int n;
			while ((n = in.read()) != -1) {
				out.write(n);
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public static void canviLider(String nomGimnas, String nouLider) throws IOException {
		File f = new File("gimnasos.txt");
		File f2 = new File("reanomenat.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);

		FileWriter fw = new FileWriter(f2, true);
		BufferedWriter bw = new BufferedWriter(fw);

		while (br.ready()) {
			String s = br.readLine();
			String[] a = s.split(";");
			if (a[0].equals(nomGimnas))
				bw.append(nomGimnas + ";" + a[1] + ";" + nouLider + ";" + 0 + "\n");
			else
				bw.append(a[0] + ";" + a[1] + ";" + a[2] + ";" + 0 + "\n");
		}
		System.out.println("Cambiado!");
		bw.flush();
		bw.close();
		br.close();
		f.delete();
		boolean success = f2.renameTo(f);
		if (success)
			System.out.println("True");
		else
			System.out.println("False");

	}

	public static void afegeixEntrenador(String nomGimnas, String nomEntrenador) throws IOException {
		File f = new File("gimnasos.txt");
		File f2 = new File("reanomenat.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);

		FileWriter fw = new FileWriter(f2, false);
		BufferedWriter bw = new BufferedWriter(fw);
		try {
			while (br.ready()) {
				String s = br.readLine();
				String[] a = s.split(";");
				if (a[0].equals(nomGimnas)) {
					s = "";
					int num = Integer.parseInt(a[3]) + 1;
					a[3] = Integer.toString(num);
					for (int i = 0; i < a.length; i++) {
						if (i != a.length - 1) {
							s += a[i] + ";";
						} else {
							s += a[i];
						}
					}
					s += ";" + nomEntrenador;
					bw.append(s + "\n");
				} else {
					bw.append(s + "\n");
				}
			}
			System.out.println("Canviat!");
			bw.flush();
			bw.close();
			br.close();
			f.delete();
			boolean success = f2.renameTo(f);
			if (success)
				System.out.println("True");
			else
				System.out.println("False");

		} catch (FileNotFoundException e) {
			System.out.println("EL FICHERO NO EXISTE");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ERROR");
			e.printStackTrace();
		}
	}

	public static void esborraGimnas(String nomGimnas) throws IOException {
		File f = new File("gimnasos.txt");
		File f2 = new File("reanomenat.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);

		FileWriter fw = new FileWriter(f2, false);
		BufferedWriter bw = new BufferedWriter(fw);
		try {
			while (br.ready()) {
				String s = br.readLine();
				String[] a = s.split(";");
				if (a[0].equals(nomGimnas)) {
					s = "";
					bw.append(s);
				} else {
					bw.append(s + "\n");
				}
			}
			System.out.println("ELIMINADO!");
			bw.flush();
			bw.close();
			br.close();
			f.delete();
			boolean success = f2.renameTo(f);
			if (success)
				System.out.println("True");
			else
				System.out.println("False");
		} catch (FileNotFoundException e) {
			System.out.println("EL FICHERO NO EXISTE");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ERROR");
			e.printStackTrace();
		}
	}

	public static void consultaEntrenadors(String nomGimnas) throws IOException {
		File f = new File("gimnasos.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String ent = "";
		try {
			while (br.ready()) {
				String s = br.readLine();
				String[] a = s.split(";");
				if (a[0].equals(nomGimnas)) {
					for (int i = 0; i < a.length; i++) {
						if (i >= 4)
							ent += a[i] + " ";
					}
				}
			}
			System.out.println("Entrenadores: " + ent);
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("EL FICHERO NO EXISTE");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ERROR");
			e.printStackTrace();
		}
	}
}
